package br.com.agenda.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.agenda.factory.ConnectionFactory;
import br.com.agenda.model.Contato;

public class ContatoDAO {
	//CRUD
	
	public void save(Contato contato) {
		String sql = "INSERT INTO contatos(nome, idade, datacadastro) VALUES (?, ?, ?)";
		
		Connection conn = null; 
		PreparedStatement pstm = null;
		try {
			// Criar conexao com o banco de dados;
			conn = ConnectionFactory.createConnectionToMySQL();
			// Criamos uma PreparedStatement, para executar uma query;
			pstm = (PreparedStatement) conn.prepareStatement(sql);
			// Adicionar os valores esperados pela Query
			pstm.setString(1, contato.getNome());
			pstm.setInt(2, contato.getIdade());
			pstm.setDate(3, new Date(contato.getDataCadastro().getTime()));
			
			// Executar a query
			pstm.execute();
			System.out.println("Contato Salvo com sucesso");
					
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			// Fechar conexoes
			try {
				if (pstm != null) {
					pstm.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
				
	}
		
	
	public void update(Contato contato) {
		String sql = "UPDATE contatos SET nome = ?, idade = ?, datacadastro = ? " +
		"WHERE id = ?";
		
		Connection conn = null;
		PreparedStatement pstm = null;
		
		try {
			conn = ConnectionFactory.createConnectionToMySQL();
			
			pstm = conn.prepareStatement(sql);
			
			// Adicionar os valores para atualizar
			
			pstm.setString(1, contato.getNome());
			pstm.setInt(2, contato.getIdade());		
			pstm.setDate(3, new Date(contato.getDataCadastro().getTime()));
			// Id do registro a ser atualizado
			pstm.setInt(4, contato.getId());
			
			pstm.execute();
			
		} catch( Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstm != null) {
					pstm.close();
				} if (conn != null) {
					pstm.close();
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void deleteByID(int id) {
		String sql = "DELETE from contatos WHERE id = ?";
		
		Connection conn = null;
		PreparedStatement pstm = null;
		
		try {
			conn = ConnectionFactory.createConnectionToMySQL();
			
			pstm = conn.prepareStatement(sql);
			
			pstm.setInt(1, id);
			
			pstm.execute();
		} catch( Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstm != null) {
					pstm.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch( Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public List<Contato> getContatos() {

		
		String sql = "SELECT * FROM contatos";
		
		List<Contato> contatos = new ArrayList<Contato>();
		
		 Connection conn = null;
		 PreparedStatement pstm = null;
		 // Resultset para receber (Recuperar) os dados do banco  *** SELECT ***
		 ResultSet rset = null;
		 
		 try {
			 conn = ConnectionFactory.createConnectionToMySQL();
			 
			 pstm = conn.prepareStatement(sql);
			 
			 rset = pstm.executeQuery();
			 // enquanto o resultset tem dado para percorrer (registros) ele roda
			 while (rset.next()) {
				
				 Contato contato = new Contato();
				
				 // Recuperar o ID
				 contato.setId(rset.getInt("id"));
				 contato.setNome(rset.getNString("nome"));
				 contato.setIdade(rset.getInt("idade"));
				 contato.setDataCadastro(rset.getDate("dataCadastro"));
				 
				 contatos.add(contato);
			}
		 } catch (Exception e) {
			 e.printStackTrace();
		 } finally {
			 try {
				 if (rset != null) {
					 rset.close();
				 }
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (conn != null) {
					 conn.close();
				 }
			 } catch (Exception e) {
				 e.printStackTrace();
			 }
		 } 
		 return contatos;
	}

	public Contato findbyID(int id) {
		String sql = "SELECT * from contatos WHERE id = ?";
		 Connection conn = null;
		 PreparedStatement pstm = null;
		 // Resultset para receber (Recuperar) os dados do banco  *** SELECT ***
		 ResultSet rset = null;
		 
		 try {
			 conn = ConnectionFactory.createConnectionToMySQL();
			 
			 pstm = conn.prepareStatement(sql);
			 
			 pstm.setInt(1, id);
			 
			 rset = pstm.executeQuery();
			 
			 if (rset.next()) {
				 Contato contato = new Contato();
				 contato.setId(rset.getInt("id"));
				 contato.setNome(rset.getString("nome"));
				 contato.setIdade(rset.getInt("idade"));
				 contato.setDataCadastro(rset.getDate("dataCadastro"));
				 return contato;
			 }
		 } catch(Exception e) {
			 e.printStackTrace();
		 } finally {
			 try {
				 if (rset != null) {
					 rset.close();
				 }
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (conn != null) {
					 conn.close();
				 }
			 } catch (Exception e) {
				 e.printStackTrace();
			 }
		 }
		return null;
	}

}
