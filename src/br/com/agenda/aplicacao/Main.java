package br.com.agenda.aplicacao;

import java.util.Date;

import br.com.agenda.dao.ContatoDAO;
import br.com.agenda.model.Contato;

public class Main {
	 public static void main(String[] args) {
		 ContatoDAO contatoDao = new ContatoDAO(); 
		 Contato contato = new Contato();
		 contato.setNome("Maria Gabriela");
		 contato.setId(55);
		 contato.setDataCadastro(new Date());
		 
//		 contatoDao.save(contato);
		 
		 // Atualizar contato
		 Contato c1 = new Contato();
		 c1.setNome("Maria Gabriela Dias Orlando");
		 c1.setIdade(37);
		 c1.setDataCadastro(new Date());
		 c1.setId(1);
		 
//		 contatoDao.update(c1);
		 
		 // Deletar o contato pelo seu Numero ID
		 Contato c2 = new Contato(); 
		 c2 = contatoDao.findbyID(3);
//		 contatoDao.deleteByID(1);
		 System.out.println("Contato " + c2.getNome());
		 for (Contato c : contatoDao.getContatos()) {
			 System.out.println("Contato: " + c.getNome());
		 }
	}
}
